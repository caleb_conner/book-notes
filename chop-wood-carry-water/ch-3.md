# Faithful in the Small Things

## Lesson

- Greatness is made of the small habits, decisions, and choices we make.
- Dream big. Start small. Be rediculously faithful.
- Focus on what you can control.
- Keep your focus on the process, while surrendering the outcome.

## Quote

> The problem with small is that it isn't sexy, and it's often repetitively boring.

> Everyone thinks greatness is sexy, it's not. It's dirty hard work
>
> -- <cite>Ben Hogan</cite>

> Everyone wants to be great until it's time to do what greatness requires.
