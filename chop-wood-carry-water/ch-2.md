# Chapter 2: Building Your Own House

## Lesson

- You are building your own house.

### Sub-Lesson

- Treat everything as an oportunity, instead of an obligation.

## Quote

> The only thing that is truly significant about today, or any other day, is who you become in the process. Each of us are building our own house. Sometimes you might think you are building for your school, your family, your company, or your team, but you are always building your own house... I hope you build wisely."

## Story

- Kota the great architect.
