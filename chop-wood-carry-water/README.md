# Chop Wood Carry Water

## by: Joshua Medcalf

- [Chapter 1: Introduction](./ch-1.md)
- [Chapter 2: Building Your Own House](./ch-2.md)
- [Chapter 3: Faithful in the Small Things](./ch-3.md)
